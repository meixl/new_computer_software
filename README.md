# idea 踩坑记录

#### IDEA如何设置取消 请不要使用行尾注释 提示

File–>settings–>Editor–>Inspections–>搜“注释”然后取消

#### 修改 idea 内存失效

https://zhuanlan.zhihu.com/p/508776219

#### idea写代码如何自动生成serialVersionUID

![x](https://jingyan.baidu.com/article/647f01152a58763e2048a874.html)

serialVersionUID

![输入图片说明](image.png)

#### 内存参数

```
-Xms512m
-Xmx4096m
-XX:+UseCompressedOops
-XX:ReservedCodeCacheSize=512m
```

#### idea 显示内存

> View -> Apperance -> Status Bar Widgets -> Memory Indicator

#### idea 调整字体大小

> Setting-> Editor-> Genearl -> `Change font size with Ctrl+Mouse Wheel in `勾选

#### idea 调整控制台颜色风格

> Editor -> Color Scheme -> Color Colors

#### redis 脚本

redis-server.exe redis.windows.conf

#### vue-cli 脚本

cmd /c vue ui

#### 介绍
记录 idea 使用的各种问题

#### 行宽

eclipse code formater `xml`文件 lineSplit 属性调整

#### 各种黄线、红线

`File`->`Setting`->`Editor`->`Inspections`

* @Autowired 黄线

    `Field injection warning`

* 注入 mapper 红线

    `Autowiring for Bean Class`

* mapper xml 黄线去除

    `No data sources configured`

    `SQL dialect detection`

* mapper xml 取消`save action`自动格式化

    找到`save action`，右下角添加 exclude

    可以参考[idea保存自动格式化屏蔽xml](https://blog.csdn.net/g363271727/article/details/127228040)

#### 生成机构树

win

cmd 跳转到所处目录

* `tree >> aaa.txt`表明输出当前结构文件夹到 aaa.txt 文件

* `tree /F >> aaa.txt`表明输出当前结构文件夹及文件夹内容到 aaa.txt 文件

#### 插件

* ignore

* CodeGlance

* Eclipse Code Formatter

* Grep Console

* IDEA Restart

* JavaDoc

* Jrebel for IntelliJ

* Key Promoter X

* Lombok

* Markdown Navigator

* Maven Helper

* Mybatis Log Plugin

* MybatisX

* Save Actions

* Scroll From Source
